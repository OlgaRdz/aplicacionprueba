import React, { useState } from 'react';
import { StyleSheet, View, Text, TextInput, Button, TouchableHighlight } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";

//hola esto es una segunda prueba de Laura
//y esto es otra prueba XD.
//Modificación de Olga
//Modificacion del lunes 19 de abril
const Formulario = () => {
    const [paciente, guardarPaciente] = useState('');
    const [propietario, guardarPropietario] = useState('');
    const [telefono, guardarTelefono] = useState('');
    const [fecha, guardarFecha] = useState('');
    const [hora, guardarHora] = useState('');
    const [sintomas, guardarSintomas] = useState('');

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const confirmarFecha = date => {
    const opciones = { year: 'numeric', month: 'long', day: '2-digit'}
    console.log(date.toLocaleDateString('es-ES', opciones));
    guardarFecha(date.toLocaleDateString('es-ES', opciones));
    hideDatePicker();
  };

  //Muestra u oculta el Time Picker
  const showTimePicker = () => {
    setTimePickerVisibility(true);
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const confirmarHora = (hora) => {
    const opciones = { hour:'numeric', minute:'2-digit', hour12:true}
    console.log(hora.toLocaleString('es-ES', opciones));
    guardarHora(hora.toLocaleString('es-ES', opciones));
    hideTimePicker();
  };
    return (
      <>
        <View style={styles.formulario}>
            <View>
                <Text style={styles.label}>Paciente:</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={ (texto) => guardarPaciente(texto) }
                />
            </View>

            <View>
                <Text style={styles.label}>Dueño:</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={ (texto) => guardarPropietario(texto) }
                />
            </View>

            <View>
                <Text style={styles.label}>Teléfono Contacto:</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={ (texto) => guardarTelefono(texto) }
                    keyboardType= 'numeric'
                />
            </View>

            <View>
              <Text style={styles.label}>Fecha:</Text>
                <Button title="Seleccionar Fecha" onPress={showDatePicker} />
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={confirmarFecha}
                    onCancel={hideDatePicker}
                />
                <Text>{fecha}</Text>
            </View>

            <View>
            <Text style={styles.label}>Hora:</Text>
                <Button title="Seleccionar Hora" onPress={showTimePicker} />
                <DateTimePickerModal
                    isVisible={isTimePickerVisible}
                    mode="time"
                    onConfirm={confirmarHora}
                    onCancel={hideTimePicker}
                    //is24Hour
                />
                <Text>{hora}</Text>
            </View>

            <View>
                <Text style={styles.label}>Síntomas:</Text>
                <TextInput
                    multiline
                    style={styles.input}
                    onChangeText={ (texto) => guardarSintomas(texto) }
                />
            </View>

            <View>
                  <TouchableHighlight onPress={confirmarFecha} style={styles.btnSubmit}>
                      <Text style={styles.textoSubmit}> Enviar </Text>
                  </TouchableHighlight>
              </View>

        </View>
      </>  
    );
}

const styles = StyleSheet.create({
    formulario: {
        backgroundColor: '#FFF',
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginHorizontal: '2.5%'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 20
    },
    input: {
        marginTop: 10,
        height: 50,
        borderColor: '#e1e1e1',
        borderWidth: 1,
        borderStyle: 'solid'
    },
    btnSubmit: {
      padding: 10,
      backgroundColor: '#06bd87',
      marginVertical: 10
    },
    textoSubmit:{
      color: '#FFF',
      fontWeight: 'bold',
      textAlign: 'center'
    }
})

export default Formulario;